#!/usr/bin/env python3
# -*- coding: utf-8 -*-


'''
Mars Rover Tests
'''


import pytest

from mars_rover import MarsRover


@pytest.fixture
def mr():
    position = [1, 1]
    direction = 'N'
    grid = [
        [False, False, False, ], 
        [True,  False, False, ],
        [False, False, False, ],
    ]
    return MarsRover(position, direction, grid)


def test_turn_left(mr):
    mr.go(['l'])
    assert mr.direction == 'W'


def test_move_forward(mr):
    mr.go(['f'])
    assert mr.position == [0, 1]


def test_move_around(mr):
    mr.go(['f', 'l', 'f', 'r', 'f', 'l', 'b', 'b'])
    assert mr.direction == 'W'
    assert mr.position == [2, 2]


def test_move_around_obstacle(mr):
    with pytest.raises(Exception) as e:
        mr.go(['l', 'f'])
    assert e.value == 'Obstacle at position [1, 0].'
    assert mr.direction == 'W'
    assert mr.position == [1, 1]
