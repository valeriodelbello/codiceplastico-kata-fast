#!/usr/bin/env python3
# -*- coding: utf-8 -*-


'''
Mars Rover
'''


class MarsRover(object):

    moves = {'N': (0, -1), 'S': (0, 1), 'E': (1, 1), 'W': (1, -1), }

    turns = {
            'l': {'N': 'W', 'S': 'E', 'E': 'N', 'W': 'S', },
            'r': {'N': 'E', 'S': 'W', 'E': 'S', 'W': 'N', },
        }

    def __init__(self, position, direction, grid):
        self.actions = {
            'f': lambda: self._move(1),
            'b': lambda: self._move(-1),
            'l': lambda: self._turn('l'),
            'r': lambda: self._turn('r'),
        }
        self.position = position
        self.direction = direction
        self.grid = grid

    def _move(self, movement):
        new_position = list(self.position)
        axis, modifier = self.moves[self.direction]
        new_position[axis] = (self.position[axis] + (modifier * movement)) % len(self.grid)
        if self.grid[new_position[0]][new_position[1]]:
            raise Exception('Obstacle at position {}.'.format(new_position))
        self.position = new_position

    def _turn(self, turn_direction):
        self.direction = self.turns[turn_direction][self.direction]

    def go(self, commands):
        for command in commands:
            self.actions[command]()
